var mongoose = require('mongoose');
var config = require('./config');
var questions = require('./data/questions.json').questions;

var assessmentController = require('./controllers/assessment.js');

var db = mongoose.connect(config.database, function(err) {
  if (err) {
      console.error('\x1b[31m', 'Could not connect to MongoDB!');
      console.log(err);
  }
});


console.log( assessmentController.getPercentileExcel( [ 15, 20, 35, 40, 50 ], [ 0.4 ]), 'should be', 29 );

console.log( assessmentController.getScore( 
	[
		[208],
		[1],
		[10],
		[1],
		[2],
		[1],
		[0],
		[1],
		[3,5,7],
		[0,7,10]
	]
));

var totals;
assessmentController.aggregateTotals( function( err, totals ){
	if(err){ throw Error( err )}
	else{
		totals = totals;
		console.log('totals aggregated');
		assessmentController.saveLatestTotals( totals, function(err){
			if(err){ throw Error( err )}
			else{
				console.log('totals saved');

				assessmentController.getBreakdown('R_8wyDwD4SXGiqxMN',function(err,breakdown){
					if(err){ throw err}
					else{
						console.log( breakdown );
						console.log( 'Breakdown Successfully created' )
					}
				});
			}
			
		});
	}
})




