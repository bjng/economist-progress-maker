/* GET home page. */

var express = require('express');
var router = express.Router();
var models = require('../models/models');
var profiles  = require('../data/profiles.json');

var assessmentController = require('../controllers/assessment');
var ObjectID = require('mongodb').ObjectID;
var _ = require('lodash');


/* GET home page. */
router.get('/', function(req, res) {
	var host = String( req.protocol + '://' + req.hostname );
	var meta = { 
		'og:url' 							: host + req.originalUrl,
		'og:title' 						: 'Economist-Citi Progress Makers at Work',
		'og:description' 			: 'Learn how progress makers are transforming business worldwide and see how you and your company compare by taking The EIU self-assessment tool, brought to you by Citi.',
		'og:imgUrl' 					: host + '/images/og-logo.jpg',
		'og:image:secure_url' : host + '/images/og-logo.jpg',
		'og:image:type'				: 'image/jpeg',
		'og:image:width'			: '300',
		'og:image:height'			: '300',
		'twitter:card'				: 'summary',
		'twitter:title' 			: 'Economist-Citi Progress Makers at Work',
		'twitter:domain'			: host + req.originalUrl,
		'twitter:image' 			: host + '/images/og-logo.jpg',
		'twitter:description'	: 'Progress makers at work: Learn about the change agents driving progress across industries and around the world'
	};
  	res.render('index', { 'title': 'Economist-Citi Progress Makers at Work',meta: meta } );
});


//app.get('/test', function(req, res) {
//  res.sendfile(__dirname + '/public/tests.html');
//});

router.get('/progress-makers', function( req, res ){
	var host = String( req.protocol + '://' + req.hostname ); 
	var meta = { 
		'og:url' 							: host + req.originalUrl,
		'og:title' 						: 'Economist-Citi Progress Makers',
		'og:description' 			: '97% of executives surveyed by The EIU say progress makers are a special breed. Meet them now and discover how their work is transforming business worldwide. Brought to you by Citi.',
		'og:imgUrl' 					: host + '/images/og-logo.jpg',
		'og:image:secure_url' : host + '/images/og-logo.jpg',
		'og:image:type'				: 'image/jpeg',
		'og:image:width'			: '300',
		'og:image:height'			: '300',
		'twitter:card'				: 'summary',
		'twitter:title' 			: 'Economist-Citi Progress Makers',
		'twitter:domain'			: host + req.originalUrl,
		'twitter:image' 			: host + '/images/og-logo.jpg',
		'twitter:description'	: 'Meet the change agents driving progress across industries and around the world'
	};
	res.render('progress-makers',{ 'title': 'Economist-Citi Progress Makers','makers': profiles, meta: meta});
});

router.get('/progress-makers/:slug', function( req,res ){
	var profile = _.findWhere( profiles, { linkUrl: req.params.slug } );
	var random = _.shuffle( _.without( profiles, profile ) ).slice(0,2);
	var host = String( req.protocol + '://' + req.hostname );
	var meta = { 
		'og:url' 							: host + req.originalUrl,
		'og:title' 						: 'Economist-Citi Progress Maker - ' + profile.name,
		'og:description' 			: 'Progress makers at work: Learn what makes them tick and how these change agents are disrupting the status quo and shaping trends in their industries and beyond.',
		'og:imgUrl' 					: host + '/images/' + profile.imgUrl,
		'og:image:secure_url' : host + '/images/' + profile.imgUrl,
		'og:image:type'				: 'image/jpeg',
		'og:image:width'			: '300',
		'og:image:height'			: '300',
		'twitter:card'				: 'summary',
		'twitter:title' 			: 'Economist-Citi Progress Maker - ' + profile.name,
		'twitter:domain'			: String( req.protocol + '://' + req.hostname + req.originalUrl ),
		'twitter:image' 			: host + '/images/' + profile.imgUrl,
		'twitter:description'	: profile.metaDescription
	};
	res.render('progress-makers-detail',{'title': profile.name, 'maker': profile,'relatedProfiles': random, meta: meta });
});

router.get('/progress-makers-report', function( req, res ){
	var host = String( req.protocol + '://' + req.hostname );	
	var meta = { 
		'og:url' 							: host + req.originalUrl,
		'og:title' 						: 'Economist-Citi Progress Maker Report',
		'og:description' 			: 'Read the Progress Maker Report...',
		'og:imgUrl' 					: host + '/images/og-logo.jpg',
		'og:image:secure_url' : host + '/images/og-logo.jpg',
		'og:image:type'				: 'image/jpeg',
		'og:image:width'			: '1200',
		'og:image:height'			: '1200',
		'twitter:card'				: 'summary',
		'twitter:title' 			: 'Economist-Citi Progress Maker Report',
		'twitter:domain'			: host + req.originalUrl,
		'twitter:image' 			: host + '/images/og-logo.jpg',
		'twitter:description'	: 'Read the Progress Maker Report...'
	};
	var random = _.shuffle( profiles ).slice(0,2);

	res.render('progress-makers-report',{ 'title': 'Economist-Citi Progress Maker Report','relatedProfiles': random, meta: meta });
});




router.get('/benchmarking-tool', function(req, res){
	var host = String( req.protocol + '://' + req.hostname );
	var meta = { 
		'og:url' 							: host + req.originalUrl,
		'og:title' 						: 'Benchmarking Tool',
		'og:description' 			: 'Some 70% of executives surveyed by The EIU recognize the importance of attracting and nurturing progress makers. Does your organization? Take our benchmarking tool to see how your company stacks up. Share the results with your LinkedIn network.',
		'og:imgUrl' 					: host + '/images/og-logo.jpg',
		'og:image:secure_url' : host + '/images/og-logo.jpg',
		'og:image:type'				: 'image/jpeg',
		'og:image:width'			: '200',
		'og:image:height'			: '122',
		'twitter:card'				: 'summary',
		'twitter:title' 			: 'Benchmarking Tool',
		'twitter:domain'			: host + req.originalUrl,
		'twitter:image' 			: host + '/images/og-logo.jpg',
		'twitter:description'	: 'Benchmarking Tool'
	};
  	res.render('assessment-tool', { meta: meta, 'title': 'Assessment' } );
});


router.get('/assessment/:id', function( req, res ){
	var random = _.shuffle( profiles ).slice(0,2);
	var id = req.params.id;
	models.Assessment.findOne( { 'id' : id }, function( err, assessment ){
		if(err){
			res.send( 400, err );
			return
		}
		if(!assessment){
			res.send( 404 );
			return
		}

		assessmentController.getBreakdown( id, function( err, breakdown ){
			if(err){
				res.send( 400, err );
				return
			}
	
			var type = assessmentController.getType( breakdown.thisType );
			var host = String( req.protocol + '://' + req.hostname );
			var meta = { 
				'og:url' 							: host + req.originalUrl,
				'og:title' 						: 'Benchmarking Tool',
				'og:description' 			: 'Try out our Benchmarking Tool...',
				'og:imgUrl' 					: host + '/images/og-logo.jpg',
				'og:image:secure_url' : host + '/images/og-logo.jpg',
				'og:image:type'				: 'image/jpeg',
				'og:image:width'			: '200',
				'og:image:height'			: '122',
				'twitter:card'				: 'summary',
				'twitter:title' 			: 'I am a ' + type.title,
				'twitter:domain'			: host + req.originalUrl,
				'twitter:image' 			: host + '/images/og-logo.jpg',
				'twitter:description'	: 'Benchmarking Tool'
			};

			res.render( 'benchmark', { 
				type: type,
				meta:meta,
				assessment: assessment,
				breakdown: breakdown,
				'relatedProfiles': random
			});

		});
		
	});
});

router.post('/assessment', function( req,res ){


	var data = {};


	data.answers = req.body.assessment;
	data.id = ( new ObjectID() ).valueOf();
  data.country = data.answers[0][0];
  data.title = data.answers[1][0];
  data.industry = data.answers[2][0];
	data.score = assessmentController.getScore( data.answers );
console.log('create',data.score );

	models.Assessment.create( data, function(err,assessment){
		
		if(err){
			res.status(400).send( err);
		}
		else{
			assessmentController.updateTotals( function(err,totals){
				if(err){ res.status(400).send( err) }
				else{
					res.json( { id:data.id } );
				}
			} );
		}
	});

});

/*exports.findOne = function( req, res ){
	models.Assessment.findOne( { '_id' : new ObjectID( req.params.id ) }, function( err, assessment ){
		res.json( assessment );
	});
};
*/
router.get('/assessment-stats/:industry?', function( req, res ){


	var stats;
	
	models.Assessment.findOne( function(err, data ){
		// data.score
	});
	
} );



router.get('/api/breakdown/:id', function(req, res) {

	assessmentController.getBreakdown( req.params.id, function(err,breakdown){
		if(err){
			res.send( 400, err );
		}
		else{
			res.json( breakdown );
		}
	} );
})


module.exports = router;








