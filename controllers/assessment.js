var _ = require('lodash'),
	models = require('../models/models'),
	fs = require('fs'),
	csv = require('csv'),
	questions = require( '../data/questions.json');
	types = require( '../data/types.json');
	arrayStream = require( 'stream-array' );


/* GENERAL CALCULATION

TotalScore=Identifying + Attracting + Developing + Leveraging															
Identifying= Q12score(identifying) + Q14score(identifying) + Q15score(identifying) + Q16score(identifying)															
Attracting= Q12score(attracting) + Q14score(attracting) + Q15score(attracting) + Q16score(attracting)															
Developing= Q12score(developing) + Q14score(developing) + Q15score(developing) + Q16score(developing)															
Leveraging= Q12score(leveraging) + Q14score(leveraging) + Q15score(leveraging) + Q16score(leveraging)															

*/


/*  e.g. question i=0 takes values from scoreMap[0] */
var whichScoreMap = [


	0,
	0,
	0,
	0,

	1,
	2,
	3
]


var lengthMap = [
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	3,
	3,
]

/*
	taken from Citi Self Asssessment Model v4 - Formulas
*/

var scoreMap = [
	[
		4,
		3,
		2,
		1,
		0
	],
	[
		4+4+4+4,
		3+3+3+3, 
		2+2+2+2,
		1+1+1+1,
		4+4+4+4,
		0,
		0,
	],
	[
		0.64 + 0.94,
		1.22 + 1.87,
		1.66 + 1.29,
		1.13 + 0.89,
		1.36 + 0.57,
		3.42 + 3.58,
		0.15	+ 0.78,
		1.40+0.84,
		0.23+0.16,
		-0.59+0.78,
		0.37 + (-0.56),
		1.00 + 0.86
	],
	[

		0.81 + 1.45,
		1.65+1.52,
		1.83+1.60,
		1.28+1.59,
		0.92+0.91,
		1.26+1.37,
		0.90+0.88,
		1.03+1.69,
		1.38+0.92,
		1.41+1.27,
		1.55+0.81,
		-2.01+ (-2.01)
	]
];

exports.getPercentileExcel = function( values, kArray){
	var sorted = [].concat(values).sort();

	results = _.map( kArray, function( val ){

		var P = val,
			N = sorted.length,
		 	k,
		 	d,
		 	n;

		 	n = P * ( N - 1 ) + 1;

		 	d = n% 1;
		 	k = Math.floor( n ) - 1;


		 	return sorted[ k ] + d * (sorted[ k + 1 ]  - sorted[ k ] );

	});

	return results;
	

}


exports.getPercentile = function ( values, kArray ){
	values = _.sort( values );

	
	results = _.map( kArray, function(){
		var k  = this,
			index = ( values.length * k ) - 1;

		if( index% 1 != 0 ){ //index is integer
			return 0.5 * ( values[index] + values[index+1] ); //average of two values
		}
		else{
			index = Math.ceil( index );
			return values[ index ]
		}

	});

	return results;
	

}



function getTypeFromPercentile( score, percentiles){

	var typeRange = [ 0 ].concat( percentiles ),
		i;

	i = _.findLastIndex( typeRange, function( range){ return range<=score }); 

	return i;
}

exports.aggregateTotals = function( callback ){

	var scoreSum=0,
		totalCount=0,
		allScores=[],
		answers = _.range(0,questions.length ),
		answersPercentage;

		_.each( answers, function( val, i){
			answers[i] = _.range(0, questions[i].length, 0);
			
		});

	var stream = models.Assessment.find().stream();

	stream.on('data', function(data){
		totalCount++;
		scoreSum+=data.score;

		allScores.push( data.score );

		_.each( data.answers, function(answersForQuestion,i){


			_.each( answersForQuestion, function( val ){
			//	console.log( answers[i][val]);
				answers[i][val]++;
			});
		});

		
	});



	stream.on('err', function(err){ 
		callback(err) 
	});

	stream.on('end', function(){

		answersPercentage = _.map(answers,function( answersForQuestions,i){
			var l = answersForQuestions.length;
console.log('integer', i, lengthMap[i]);
			return _.map( answersForQuestions, function( count ){
				return Math.round( (count/totalCount/lengthMap[i] ) * 100 );//( Math.round( ( count/totalCount) * 100 * 100) / 100 ).toFixed(2); //reduce to two rounded decim
			});
		});

		var percentiles = exports.getPercentileExcel( allScores,[0.2,0.4,0.6,0.8] ),
		 	typeCount = _.range(0,percentiles.length+1,0);

		_.each(allScores, function(score){ //closest percentil
			typeCount[ getTypeFromPercentile( score, percentiles ) ]++;
		});

		var data = {
			 
			scoreSum: scoreSum,
			totalCount: totalCount,
			percentiles: percentiles,
			typeCount: typeCount,
			answers: answers,
			answersPercentage: answersPercentage

		};

		callback( null,data );
	
	});

}

exports.saveLatestTotals = function( data,callback ){
		models.Totals.findOneAndUpdate( {}, 
			data,
			{upsert:true}
			,callback );
}

exports.getLatestTotals = function( callback ){


	models.Totals.findOne({}, function(err,totals){
		if(err){ callback(err) }
		else if(!totals){ 
			exports.aggregateTotals( function(err,data){
				if(err){ callback(err) }
				else{
					exports.saveLatestTotals( data, function(err){
						if(err){callback(err);}
						else{callback(null, data )}
					});
				}
			} );
		}
		else{ callback(null, totals) };
	});

}

exports.updateTotals = function( callback ){
	exports.aggregateTotals( function( err, totals ){
		if(err){ callback(err) }
		else{
			exports.saveLatestTotals( totals, function( err ){
				if(err){ callback( err ) }
				else{
					callback( null,totals );
				}
			});
		}
	} );
}

exports.getBreakdown = function( id, callback ){
	models.Assessment.findOne({ 'id':id }, function(err,assessment){
		if(err){ callback(err) }
		else{
			exports.getLatestTotals( function(err,totals){
				if(err){ callback( err )}
				else{
					breakdown = totals.toObject();

					breakdown.thisAnswers = assessment.answers,
					breakdown.thisType = getTypeFromPercentile( assessment.score,totals.percentiles),
					breakdown.thisScore = assessment.score;
					callback( null, breakdown )
				}
			})

		}
	});
}

exports.exportAll = function( callback ){
	var stream = models.Assessment.find().stream();


	stream.on('data', function(data){

	});
	stream.on('err', function(err){ callback(err) });

	stream.on('end', function(){
		
	});

}

exports.getType = function( i ){
	return types[ i ] || null
}

exports.getScore = function( answers  ){

	var score = 0;

	var scoringQuestions = answers.slice(3);
console.log(answers);
	_.each( scoringQuestions, function(answersForQuestion,i){
	
		var map = scoreMap[ whichScoreMap[ i ] ];

		_.each( answersForQuestion, function( val ){
			score += map[ val ];
		});

	});

	return score;

};



