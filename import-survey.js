var fs = require('fs'),
	mongoose = require('mongoose'),
	_ = require('lodash'),
	models = require('./models/models'),
	questions = require('./data/questions.json'),
	assessmentController = require('./controllers/assessment'),
	csv = require('csv'),
	config = require( './config');


mongoose.connect(config.database);

var columNamesToImport=[
		
		'Q401',
		'Q460',
		'Q450',

		'Q12_1',
		'Q12_2',
		'Q12_3',
		'Q12_4',
		
		'Q14',

		'Q15_14','Q15_15','Q15_16','Q15_17','Q15_18','Q15_19','Q15_20','Q15_21','Q15_22','Q15_23','Q15_24','Q15_25',
		//'Q15_26',
		//'Q15_26_TEXT'

		'Q16_14','Q16_15','Q16_16',	'Q16_17','Q16_18',	'Q16_19','Q16_20','Q16_21','Q16_22','Q16_23','Q16_24','Q16_26',
		//'Q16_25','Q16_25_TEXT'

	];


var ignoreIfSelected = [
	'Q15_26_TEXT',
	'Q16_25_TEXT'
]

var questionMap = [

	0,
	1,
	2,

	3,
	4,
	5,
	6,

	7,

	8,8,8,8,8,8,8,8,8,8,8,8,

	9,9,9,9,9,9,9,9,9,9,9,9
]







var aggregated = [];
var i = -1;



function validateData( data ){

	var answers = data.answers;
	var valid = true;


	if( answers.length != 10 ){ valid = false }
	

	if( answers[0].length != 1 || 
		answers[1].length != 1 || 
		answers[2].length != 1 ||
		answers[3].length != 1 ||
		answers[4].length != 1 ||
		answers[5].length != 1 ||
		answers[6].length != 1 ||
		answers[7].length != 1 ||
		answers[8].length != 3  ||
		( answers[9].length == 0  || answers[9].length > 3 ) ){

			valid = false
	}

	if(!valid){console.log( 'invalid:', data.id )};

	return valid

}


var reader = fs.createReadStream('./import_allresults.csv');
var parser = csv.parse({trim:true, columns:true});
var transform = csv.transform( function( record ){
		i ++;

		var ignoreRecord = false;



		_.each( ignoreIfSelected, function( name ){
				ignoreRecord = ignoreRecord || ( record[ name ] && record[ name ] != '' ) ;
		});

		if( ignoreRecord ){ 
			console.log("ignored:", record['V1'] );
			return null;
		}


		var answers = [];
		

		_.each( columNamesToImport, function( name,i){

	

			if(! answers[ questionMap[i] ]  ){
				 answers[ questionMap[i] ] = [];
			}
		
			if( !record[name] ){
				return
			}

			//invalid value fixing
			var val = record[name].replace( /\s+/g,' ');
				val = val.replace( 'etc)', 'etc.)' );
				val = val.replace( /^\s|\s$/g,'');
				val = val.replace( /^Dynamic professional development \(rotation between departments, fast tra$/gm,'Dynamic professional development (rotation between departments, fast tracking promotions)')
				val = val.replace( /^Flexible work environment \(w$/gm, "Flexible work environment (working from home, work/life balance, etc.)")
		

			var qi =  questions[ questionMap[i] ].indexOf( val );

			if( qi == -1 ){
				console.log( "can't find valid answer for", name, '"'+record[name]+'"', questionMap[i], questions[ questionMap[i]]  );
			}


			answers[ questionMap[i] ].push(  qi );

			
		});

		if( record['Q16_26'].search('in particular') != -1){
			console.log( 'q16 found', answers);
		}


		var data = {
			id: record['V1'],
			country: answers[0],
			title: answers[1],
			industry: answers[2],
			answers: answers,
			score: assessmentController.getScore( answers )
		}

		if( ! validateData( data )){
			return null
		}


		aggregated.push( data );
		return data;
	})


reader.pipe( parser ).pipe( transform );
transform.on('finish', function() {


	console.log('aggregated:', aggregated.length );

	models.Assessment.create( aggregated, function(err){

	 		console.log( String(aggregated.length), 'Sessions imported');
	 		mongoose.disconnect();
	 })
 	console.log(aggregated[0]);


});
