'use strict';

var gulp = require('gulp');
var gulpFilter = require('gulp-filter');
var server = require('gulp-express');


// load plugins
var $ = require('gulp-load-plugins')();

gulp.task('styles', function () {
    return gulp.src('public/stylesheets/style.styl')
        .pipe($.stylus() )
        .pipe(gulp.dest('public/stylesheets/'));

});

gulp.task('scripts', function () {

	var filter = gulpFilter(['*', '!public/javascript/libs','!public/javascript/tests' ]);

    return gulp.src('public/javascripts/**/*.js')
    	.pipe(filter)
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe( filter.restore() )
        .pipe(gulp.dest('public/dist'))
        .pipe($.size());
});


//''templates','styles', 'scripts'
gulp.task('compile', ['styles','scripts'], function () {
    return;

    var assets = $.useref.assets({searchPath: ['.tmp','app'] });
    var gulpif = require('gulp-if');
    var minifyHtml = require('gulp-minify-html');

    return gulp.src('.tmp/*.html')
        .pipe( assets )
        .pipe( gulpif( '*.js', $.uglify() ))
        .pipe( gulpif( '*.css', $.csso() ))
        .pipe( assets.restore() )
        .pipe( $.useref() )
        //.pipe( minifyHtml( {empty:true} ) )
        .pipe(gulp.dest('dist'))
        .pipe($.size());
});


gulp.task('test',function(cb){
	console.log('hello');
});




gulp.task('serve', function () {

	server.run({
        file: './bin/www'
    });



	gulp.watch(['views/**/*.jade'], server.notify);
   	gulp.watch(['public/stylesheets/**/*.styl'], ['styles']);
    gulp.watch(['public/stylesheets/**/*.css'],  server.notify);
    gulp.watch(['public/javascripts/**/*.js'], ['scripts']);
    gulp.watch(['public/dist/**/*.js'], server.notify);
   	gulp.watch(['public/images/**/*'], server.notify);
    gulp.watch(['app.js', 'routes/**/*.js', 'models/**/*.js', 'controllers/**/*.js'], [server.run]);

})

gulp.task('open', function () {
    require('opn')('http://localhost:3000');
});


gulp.task('build', [ 'compile']);

gulp.task('default', ['watch'] );

gulp.task('watch', ['styles','scripts','serve', 'open'], function () {
   

});

