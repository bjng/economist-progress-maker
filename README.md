# Models

### User 

```
{
	session id:""
	timestamp:null,
	date:"",
	name:"",
	answers: [
		{ 
			question: "",
			answer: "",
		}
	]
	result: 0,	
}

```

### AssesmentModel

```
{
	
	chapters: [
		{ 	name:"",
			description:"",
			tooltip:""
			questions:[
				{ 
					text:"",
					type:"",
					tooltip:"",
					answers: [ { text:"", value:0 }, ...]
				},...
			]

		},
		...
	]
}

```

### BadgeModel

```
{
	id: ''
	title:'',
	description:'',
	imageUrl:''
}

```

### MakerModel

```

{

	id:"",
	name:"",
	title:"",
	imageUrl:""
	description:"",
	body:"",
	download:"",
	tags:""

}



```

# Todo

* check how ember hooks controllers to views/ templates