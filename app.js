var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongoose = require('mongoose');


var jadeDynamicIncludes = require('jade-dynamic-includes');


var app = express();



var config = require('./config');
console.log( config);

var db = mongoose.connect(config.database, function(err) {
  if (err) {
      console.error('\x1b[31m', 'Could not connect to MongoDB!');
      console.log(err);
     // throw Error( );

  }
});

var routes = require('./routes/index');
var users = require('./routes/users');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


jadeDynamicIncludes.initTemplates(__dirname + '/views/profiles',true);


app.use(require('connect-livereload')());

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(jadeDynamicIncludes.attachTemplatesToRequest());
app.use('/', routes);
app.use('/users', users);


app.locals.moment = require('moment');

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// var server = app.listen(port, function() {
//     console.log('Listening on port %d', server.address().port);
// });



module.exports = exports.app  = app;
