
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	Mixed = mongoose.Schema.Types.Mixed,
	ObjectId = mongoose.Schema.ObjectId;


var self = {
	Assessment: mongoose.model('Assessment', new Schema({
			industry: String,
			country: String,
			title:String,
			answers: [],
			date: {type: Date, default: Date.now},
			id:String,
			score: Number
		})),
	Totals: mongoose.model( 'Totals', new Schema({
			date: {type: Date, default: Date.now},
			scoreSum: Number,
			totalCount: Number,
			percentiles: Array,
			typeCount: Array,
			answers: Array,
			answersPercentage: Array
		})),
	Question : mongoose.model('Question', new Schema({
			id:Number,
			type:String,
			text:String,
			max:Number,
			required:Number,
			other: String,
			answers:[]
	})),
	AnswerCount : mongoose.model('AnswerCount', new Schema({
		question:Number,
		answer: String,
		total : Number,
		industry: String,
		title:String,
		country:String, 
	})),
	Types:  mongoose.model('Type', new Schema({
			title: String,
			description : String,
		})),
	Profile : mongoose.model('Profile', new Schema({
			name:String,
			industry:String,
			description: String,
			linkUrl:String,
			imgUrl:String,
			body : String,
			report: Mixed,
		}))
}



module.exports = self;




