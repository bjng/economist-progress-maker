(function($) {
	
	// Foreach Polyfil
	if(!Array.prototype.forEach){
		Array.prototype.forEach = function(callback, thisArg) {
			if(typeof(callback) !== "function") {
				throw new TypeError(callback + " is not a function!");
			}
			var len = this.length;
			for(var i = 0; i < len; i++) {
				callback.call(thisArg, this[i], i, this)
			}
		}
	}


	$(document).ready(function(){
		
		//DISABLE TAB INDEX
		$('a, input').each(function(){
				this.tabIndex = -1;
		});

		//Click to expand social menu
		$('.social-btn').on('click', function(){
			$('.social-links').toggleClass('open');
		});


		//Mobile menu

		$('.btn-mobile-menu').on('click', function(){
			$('nav.navigation, .btn-mobile-menu').toggleClass('open');
		});
		

		// MOBILE DETECTION
		if ( Modernizr.touch ) { 
			
			$('.content').addClass('mobile');

			if ( $(window).width() < 600 ) {
				//alert('mobile is working!!');

				// check orientation on page load
				checkOrientation();

				// listen for orientation changes
				$(window).on('orientationchange', function() {
					checkOrientation();
				});

			}
		} else {
			//console.log("desktop");
			// Hover states of buttons
			$('.btn, .social-links .social-btn').on('mouseenter', function(){
				$(this).addClass('over');
				$(this).on('mouseleave', function(){
					$(this).removeClass('over');
				});
			});

		}

		// DEVICE ORIENTATION
		function checkOrientation() {
			var o = window.orientation;

			if (o != 90 && o != -90) {
				$('.rotate-device').fadeOut();// portrait
				//alert('portrait');
			} else {
				$('.rotate-device').fadeIn();// landscape
				//alert('landscape');
			}
		}
	});

}( jQuery ));


