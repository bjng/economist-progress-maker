
var questions = [
[
//"Which of the following best describes your title?",
"Board member",
"CEO/President/Managing director",
"CFO/Treasurer/Comptroller",
"CIO/Technology director",
"Human resources director",
"Other C-level executive",
"SVP/VP/Director",
"Head of business unit",
"Head of department",
"Manager",
"Other (managerial)"
],
[
//"What is your primary industry?",
"Aerospace and defence",
"Agriculture and agribusiness",
"Automotive",
"Chemicals",
"Construction",
"Real estate",
"Consumer goods",
"Education",
"Energy and natural resources",
"Entertainment, media and publishing",
"Financial services",
"Government/public sector",
"Healthcare, pharmaceuticals and biotechnology",
"IT and technology",
"Logistics and distribution",
"Manufacturing",
"Professional services",
"Retail",
"Telecoms",
"Transportation, travel and tourism"
],
[ "Very strong", "Somewhat strong", "Neither strong nor weak", "Somewhat weak", "Very weak"],
[ "Very strong", "Somewhat strong", "Neither strong nor weak", "Somewhat weak", "Very weak"],
[ "Very strong", "Somewhat strong", "Neither strong nor weak", "Somewhat weak", "Very weak"],
[ "Very strong", "Somewhat strong", "Neither strong nor weak", "Somewhat weak", "Very weak"],
[
//"When was your organisation’s strategy for talent development most recently updated?",
"Within the last year",
"One to two years ago",
"Three to five years ago",
"More than five years ago",
"We are currently updating it",
"We don't have a strategy in place",
"I don't know"
],
[
//"How does your organisation attract progress makers?",
"Merger and acquisitions",
"Crowdsourcing",
"Social media",
"Networking",
"Partnership and mentorship programmes",
"Venture capitalist endeavours in new projects and ideas",
"Attractive financial packages for our employees",
"Encouraging brainstorming and knowledge exchange",
"Providing training opportunities and conferences (to employees and any interested parties)",
"Providing global opportunities",
"Flexible work environment (working from home, work/life balance, etc.)",
"Work environment that promotes innovation and leadership"
],
[
//"What are the most important elements your organisation can provide progress makers to succeed?", CHANGED TO: "As a decision-maker, what do you think are the most important elements your organisation can provide progress makers to succeed?"
"Independence",
"Access to financial resources",
"Access to human resources and skills",
"Access to diverse points of view",
"Access to knowledge and information",
"Recognition and exposure",
"Responsibility for their own projects",
"Opportunities to take on challenges",
"Flexible work environment (working from home, work/life balance, etc.)",
"Dynamic professional development (rotation between departments, fast tracking promotions)",
"Opportunities for team building",
"Nothing in particular - a progress maker is just like anyone else"
]
]
var slide = 1;
$(document).ready(function(){
	$('.slide[data-slide-id="1"]').show();
	//$('.slides .slide[data-slide-id="1"]').velocity("fadeIn", { duration: 300 });
	if(window.location.hash != ''){
		$('.tabs .tab-content[data-href="' + window.location.hash + '"]').show();
		$('.tabs header ul li a[href="' + window.location.hash + '"]').parent().addClass('active');
	}
	else{
		$('.tabs .tab-content').first().show();
		$('.tabs header ul li:first-child').addClass('active');
	}
	$('.tabs header ul li').on('click', function(){
		$('.tabs header ul li').removeClass('active');
		$(this).addClass('active');
		var href = $(this).find('a').attr('href');
		//window.location.hash = href;
		/*$('.slides .slide').hide();
		$('.slide[data-slide-id="1"').show();*/


		/*$('.slides .slide').velocity("fadeOut", { duration: 300, 
			complete: function(){
				$('.slide[data-slide-id="' + slide + '"]').velocity("fadeIn", { duration: 300 });
			}
		});*/
	$('.tabs .tab-content').hide();
	$('.tabs .tab-content[data-href="' + href + '"]').show();
	return false;
});

	$('.next-slide').on('click', function(){
		slide++;
		//changeSlide(slide);
	});

	$('.prev-slide').on('click', function(){
		slide--;
		//changeSlide(slide);
	});
	$('.progress-bar .indicator-bar-parts div').on('click', function(){
		slide = $(this).data('id');
	});


	$('.view-breakdown').on('click', function(){
		$('.tabs header ul li a[href="#breakdown"]').parent().trigger('click');
	});
	generateCharts();

})

function generateCharts(){
	$.getJSON('/api/breakdown/' + window.location.pathname.split('/')[2], function (data){
		// RESULTS OVERVIEW
		var chart1 = $('.chart-1');
		var maxValue = 0;
		var minValue = 0;
		var fx = 0;
		var maxFx = 0;
		for(var i = 0; i < data.typeCount.length; i++){
			if(data.typeCount[i] > maxValue){
				maxValue = data.typeCount[i];
				minValue = maxValue;
			}
			if(data.typeCount[i] < minValue){
				minValue = data.typeCount[i];
			}
			fx = ( (data.typeCount[i] - minValue) / minValue ) * 90 + 10;
			if(fx > maxFx){
				maxFx = fx;
			}
		}
		
		//console.log(minValue, maxValue);
		var multiplier = 250 / maxFx;
		var vAxisStep = Math.ceil(maxValue / 10 / 5) * 10;
		for(var i = 0; i < 5; i++){
			//chart1.find('ul#vAxis').prepend('<li><span>' + (vAxisStep * i) + '</span></li>');
			chart1.find('ul#vAxis').prepend('<li><span></span></li>');
		}
		
		//console.log(fx);

		for(var i = 0; i < data.typeCount.length; i++){		
			fx =   ( ( ( data.typeCount[i] - minValue ) / (maxValue-minValue) ) * 60) + 40;
			//console.log(fx);
			chart1.find('ul#bars').append('<li><div class="bar bar-' + (i+1) + '" style="height: ' + fx  + '%"><span>' + (i + 1) + '</span></div></li>');
		}
		chart1.find('ul#bars li').eq(data.thisType).find('.bar').addClass('active');
		
		var resizeTimeout;
		drawChartsFromData(data);
		window.onresize = function(){
			clearTimeout(resizeTimeout);
			drawChartsFromData(data);
		};
		$('.tabs header ul li').on('click', function(){
			drawChartsFromData(data);
		});

		$('.next-slide, .prev-slide, .progress-bar .indicator-bar-parts div').on('click', function(){
			if(slide > 2)
				var id = slide + 3;
			else
				id = slide;
			changeSlide(slide , function(){
				if(slide !== 3){
					drawChart('.slide-chart-' + (slide + 1), 'chart-' + (slide + 1), questions[id - 1], data, id );
				}
			});
		});

	})
}

function changeSlide(slide, callback){
	//console.log(slide);
	$('.slide').hide();
	$('.slide[data-slide-id="' + slide + '"]').css({ opacity: 0, "display": "block"});
	callback();

	$('.slide[data-slide-id="' + slide + '"]').velocity({ opacity: 1 }, { display: "block" });
	
	//	callback();
	//$('.slide[data-slide-id="' + slide + '"]').velocity("fadeIn", { delay: 300, duration: 300 });
	var progressWidth = 0;
	var progressWidth = 100/ (6-1) * ( slide - 1 );

	$('.progress-bar .indicator-bar').velocity({'width': progressWidth + '%'}, { duration: 1000 });
}

function drawChartsFromData(data){
	drawChart('.slide-chart-2', 'chart-2', questions[0], data, 1);
	drawTable('.slide-chart-4', data);
}
function drawCharts(){
}



function drawTable(wrapper, json){
	var titles = ['1 - Identifying progress makers', '2 - Attracting progress makers', '3 - Developing progress makers', '4 - Leveraging progress makers'];
	var classes = ['first', 'second', 'third', 'fourth', 'fifth']
	var elements = $(wrapper + ' .results-page-matrix >ul');
	elements.find('>li >ul:not(.header)').parent().remove();
	for(var i = 0; i < titles.length; i++){
		var content = '';
		for(var j = 0; j < 5; j++){
			if(j == json.thisAnswers[i+3][0])
				var active = true;
			else
				var active = false;
			content += '<li' + (active? " class='active'": "") + '><span>' + json.answersPercentage[i + 3][j] + '%</span></li>';
		}
		elements.append('<li><ul><li><span>' + titles[i] + '</span></li><li><ul class="' + classes[i] + '">' + content + '</ul></li></ul></li>');
	}
	
}
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawCharts);


function drawChart(wrapper, div, strings, json, id) {

	var thisAnswer = json.thisAnswers[id];

//	var percentage = '';
	var selectedColors = ['#00BAF3', '#0186D0', '#00529A'];
	//var max = 0;
	//var min = 0;
	//var multiAnswer = '';
//	var singleAnswer = '';
//	var maxColor;

	var sortedAnswers = [];

	for(var i = 0; i < thisAnswer.length; i++){
		sortedAnswers.push({
			index: i, 
			percentage : json.answersPercentage[id][ thisAnswer[i] ],
			text: questions[ id-1 ][ thisAnswer[i] ],
			color: selectedColors[i]
		});
	}
	
	sortedAnswers.sort( function( a,b ){ return b.percentage - a.percentage });

	var html = '';

	if(sortedAnswers.length > 1 ){
		for( var i = 0; i < sortedAnswers.length; i++){
				var answer = sortedAnswers[i];
				html += '<p><span style="color: ' + answer.color + '">' + Math.round(answer.percentage) + '%</span> of the respondents also use <span style="color: ' + answer.color + '">' + answer.text + ' </span></p>';
		}
	}
	else{
		var answer = sortedAnswers[0];
		html+= '<p>Your answer matches <span style="color:' + answer.color + '">' + Math.round( answer.percentage ) + '%</span> of the survey panel. Scroll over the doughnut to see how the other respondents answered.</p>';
	}
	$(wrapper + ' .your-answer').html( html );

	//console.log(html);

	var data = google.visualization.arrayToDataTable([]);
	data.addColumn('string', strings[0]);
	data.addColumn('number', '%');

	for(var i = 0; i < strings.length; i++){
		data.addRow( [ strings[i], json.answers[id][i] ] );
	}

	var currentColor = 0;
	var colors = ['#d23399', '#d0de33', '#a13d79', '#f2a233', '#b37ec4', '#348f92', '#fed732', '#349d64', '#d58145', '#33c0c7', '#d23399', '#d0de33', '#a13d79', '#f2a233', '#b37ec4', '#348f92', '#fed732', '#349d64', '#d58145', '#33c0c7'];
	var slices = {};
	for(var i = 0; i < thisAnswer.length; i++ ){
		colors[thisAnswer[i]] = selectedColors[currentColor];
		slices[thisAnswer[i]] = {offset: 0.1}
		currentColor++;
	}

	var options = {
		legend:{ position: 'none'	},
		tooltip: { textStyle:{ color: '#00BAF3'}},
		pieHole: 0.4,
		pieSliceText: 'none',
		slices: slices,
		colors: colors,
		chartArea: { height: '80%', width: '80%'},
		dataLabelFormatString: '%.2f %'
	};

	$(wrapper + ' .legend-cols').html('<ul></ul>');
	for(var i = 0; i < strings.length; i++){
		//console.log(thisAnswer);
		if($.inArray(i, thisAnswer) !== -1){
			//console.log(i);
			$(wrapper + ' .legend-cols ul').append('<li><span class="marker" style="background: ' + options.colors[i] + '"></span><span class="text" style="color: ' + options.colors[i] + '">' + strings[i] + '</span></li>');
		} else{
			$(wrapper + ' .legend-cols ul').append('<li><span class="marker" style="background: ' + options.colors[i] + '"></span><span class="text">' + strings[i] + '</span></li>');
		}
	}
	
	
	var chart = new google.visualization.PieChart(document.getElementById(div));
	chart.draw(data, options);
}