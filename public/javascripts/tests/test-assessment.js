describe("The Assesment Tool", function() {
  it("should be registered as assesment", function() {
    expect( AssessmentTool ).toBeDefined( true );
  });

});

describe("An Iterator", function() {

	var Iterator = AssessmentTool.Iterator;
	var iter;


	it("should throw an error if no data passed to constructor", function() {
		expect( Iterator ).toThrowError();
	});

	it("should have data when data passed to constructor", function() {

		expect( function(){
			iter = new Iterator( ['test'] );
		} ).not.toThrowError();

		expect( iter.data[0] ).toEqual( 'test' );

	});

	///

	it("should return the first item on next", function() {
		expect( iter.hasNext() ).toBeTruthy()
		expect( iter.next() ).not.toBeNull();
	});

	it("should return null on next if has no next", function() {
		expect( iter.hasNext() ).not.toBeTruthy()
		expect( iter.next() ).toBeNull();
	});



});