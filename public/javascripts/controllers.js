App.Controller = OO.Object.extend({
	init: function(){
		var self = this;
		$( window ).resize( function(){
			self.updateDisplay();
		});
		//console.log('init 1');
		this._super();
	},
	createChildren:function(){
		//console.log('super');
	},
	updateDisplay:function(){},
});


App.TimelineController = App.Controller.extend({
	init: function(){
		this.latestIndex = 0;
		this.total = this.model[ this.model.length-1 ];
		this._super();
		this.createChildren();
	
	},
	createChildren: function(){
		var self = this;
		//console.log(this.model, this.view);
		
		//list items and event listeners

		var dx = 0;


		self.model.forEach( function( marker,i){

			var pos = ( marker / self.total ) * 100;

			var point = $('<span style="left:' + pos + '%" class="disabled"></span>');
			if (i == self.model.length - 1) 
				point.addClass( 'last-point');

			point.hammer().on('tap', function(){
				if( $(this.index) > self.latestIndex ) return;
				App.sendAction( 'seek', self.model[ $(this).index() ] );
				
			});
			$('.timeline-parts', self.view).append(point);

		})

	},
	seek: function( i ){

		var self = this;

		this.latestIndex = Math.max( this.latestIndex, i );

		var active = _.where( self.model, function( marker ){ return  marker <= self.latestIndex } );

		_.each( active, function( marker){
			$('.timeline-container span',self.view).eq( _.indexOf(self.model,  marker ) ).removeClass( 'disabled' );
		});

		var progressWidth = ( this.latestIndex / self.total ) * 100;
		var positionWidth = ( i / self.total ) * 100;

		$('.timeline-progress').velocity({'width': progressWidth + '%'}, { duration: 1000, queue: false });
		$('.timeline-position').velocity({'width': positionWidth + '%'}, { duration: 1000, queue: false });

	}
});


App.QaController = App.Controller.extend({
	init:function(){
		this.value = [];
		this._super();
	},
	createChildren: function(){
		var self = this;
		this._super();
	},
	isValid:function(){
		return this.value && this.value.length > 0;
	},
	sendAction: function(action, params ){
		App.sendAction(action,params)
	},
	setValue: function( value ){
		this.value = value;
		this.sendAction('updateQuestion');
	},
	getValue: function(){ return this.value },
});

App.QaBreakerController = App.QaController.extend({
	createChildren:function(){
		this._super();
	}
});

App.QaSliderController = App.QaController.extend({
	init: function(){
	
		this._super();
		this.selected = 0;
		this._snapPoints = [];
		this.value= [ 0 ];
		
	},
	isMobile: function(){
		return $(window).width() < 600;
	},
	createChildren: function(){
				
		var self = this;
		var view = $(this.view).first();
    	
    	$('h1',view).text( self.model.text );

	    var handle = $('.handle', self.view);

	    $('.input',this.view).hammer().on("drag tap release", function(ev) {
	    	if(self.isMobile()){  return }
	    	ev.gesture.preventDefault();

        var touches = ev.gesture.touches, 
        	w = view.width(), 
        	x = ev.gesture.center.pageX - $( this ).offset().left,
   			snapPoints = self._snapPoints,
   			closestX=snapPoints[0],
   			snapMin,
   			snapMax;

	   		//x = .5*handle.width();

	    	snapPoints.forEach( function( px ){
	    		if(Math.abs( px - x ) < Math.abs( closestX - x ) )
	    			closestX = px;
	    	});
       	
				snapMin = closestX - 10;
      	snapMax = closestX + 10;

 
      	if( x>snapMin && x<snapMax || ev.type == 'release'){
      		x = closestX;
      	}
 	   
				x = Math.max( Math.min( x,w) ,0);
			
				if(ev.type == 'release'){
					self.selected = _.indexOf( snapPoints, closestX );
					self.setValue( [ self.selected ] );
					
					var items = $('li',self.view);
					items.removeClass( 'selected');
					
					setTimeout(function(){
						items.eq(self.selected).addClass( 'selected');
					},1000);
					
					//handle.velocity({ 'left' : x - .5*handle.width() + 'px' }, { duration: 1000 });
				}
				handle.css( 'left', x - .5*handle.width() + 'px');
	    
	    });

	    //$('.slider-handle', this.view).hammer().on("release", function(ev) {
	    //    self.updateDisplay();
	    //});
		
		//ANSWER LABELS

			var self = this; //==qaSliderController

	    this.model.answers.forEach( function( answer,i){
	    	var labelText = $('<li><span>' + answer + '</span></li>');
				$(labelText, this.view).hammer().on('tap', function(){
					if( !self.isMobile() ){ return }
					labelText.siblings().removeClass('selected');	
					labelText.addClass('selected');
					self.selected = labelText.index();
					self.setValue( [ self.selected  ] );
				});
	    	$('ul', self.view).append(labelText);
	    });

		setTimeout( function(){
			self.updateDisplay();
		},200);

	    this._super();

	},
	setSnapPoints: function(){

		var snapPoints = [],
			w = this.view.width(),
			buttons = $('ul li',this.view),
			bw = buttons.first().outerWidth(),
			padX = ( w - ( bw * buttons.length) ) / ( buttons.length + 1),
			dx = padX,
			li;

			//console.log(w);

		buttons.each( function(i){
			li = $(this);
			li.css( 'left', dx +'px' );
			snapPoints.push( dx + .5*bw );
			dx += bw+padX;
		});

		this._snapPoints = snapPoints;

		return snapPoints
	},
	updateDisplay:function(){
		//$('.title', this.view).text( this.model.text );
		
		var snapPoints = this.setSnapPoints();

		var pos = snapPoints[ this.selected ] || 0;
		
		var handle = $('.handle',this.view);
		handle.css({ 'left' : (pos - .5*handle.width()) +'px' } );

	
		this._super();

	}
});

App.QaListController = App.QaController.extend( {
	init: function(){
		this._super();
		this.max = this.model.max || 0;
		this.required = this.model.required || 1;
	},
	isValid:function(){
		
		if( this.value && this.value.length == 0 && this.required && this.required == 0 ){ return true }
		if( this.required && this.value && this.value.length >= this.required){
			if(!this.max){ return true }
			else if(this.max >= this.value.length){ return true }
		}
		return false
	},
	createChildren: function(){
		var self = this;

		var ul = $('ul', self.view),
			li;

		this.model.answers.forEach( function( answer,i ){
			li = $('<li/>');
			li.append( $('<i/>') );
			li.append( $('<span>' + answer + '<span/>' ) );
			ul.append(li);
		});

		if(this.model.other){
			var inputField = '<li class="other"><i></i>Other<input placeholder="Please specify..." /></li>';
			ul.append( inputField );
		}

		ul.on( 'click','li', function(){ 
			self.toggleSelection( $(this).index() );
		});
		this._super();
	},
	toggleSelection: function( i ){
		var items = this.model.answers;
		var item = items[i];
		var val = this.getValue();
		
		if( _.contains(val,i) && this.model.answers.length > 2  ){
			$( 'ul li', this.view ).eq(i).removeClass('selected');
			val = _.without( val, i );
			//style removeClass selected
		}
		else if( val.length < this.max || this.max == 0 ){
			$( 'ul li', this.view ).eq(i).addClass('selected');
			val.push(i);
		}
		else if( this.max == 1 && val.length == 1){
			$( 'ul li', this.view ).removeClass('selected');
			$( 'ul li', this.view ).eq(i).addClass('selected');
			val = [ i ];	
		}

		this.setValue( val );
	}

});

App.QaDropdownController = App.QaController.extend( {
	init: function(){
		this._super();
		this._scrollValue = 0;
		this.isScrolling = false;
		this.isOpen = false;
		this.index = -1;
	},
	createChildren: function(){
		var self = this;

		var ul = $('ul',this.view);
		var button = $( 'button',this.view );
		var scrollHandle = $('.handle',this.view);
		var scrollBar    = $('.scrollbar', this.view);
		var dropdown = $( '.QaDropdown-inner',this.view );
		var isScrolling = false;
		var showHandle  = false;
		
		var ulHeight, 
			handleHeight, 
			ddHeight = 0;

		button.children( 'span' ).html( this.model.nullValue );
		
		this.model.answers = this.model.answers.sort();

		this.model.answers.forEach( function( answer ){
			ul.append( $('<li>' + answer + '</li>') );
		});
		
		var dropdownItems = $( 'ul.items li',this.view );
	
		handleHeight = Math.round( dropdown.height() / ( ul.outerHeight() / dropdown.height() ) );

		scrollHandle.height( handleHeight );

		
		// click list item
		dropdownItems.on('click', function(e){

			if(self.isScrolling) return;

			self.selectItemByIndex( $(this).index() );
			
			self.close();
		});	


		//open dropdown and manage close events
		button.on( 'click', function(e){

			e.stopImmediatePropagation();
			self.toggleDropdown();		
		});

		var scrollByEvent = function( event, inverse ){

			event.gesture.preventDefault();
			self.isScrolling = true;
			var dy = inverse ?  event.gesture.deltaY :  -event.gesture.deltaY,
				scrollVelocity = 0,
				ny = Math.max(0, Math.min( 1, self.getScrollValue() + ( dy / scrollVelocity ) ) );

			self.setScrollValue( ny );
		}

		var handleTimeout;

		var showHandle = function(){
			scrollBar.addClass('visible');
			clearTimeout( handleTimeout );
			handleTimeout = setTimeout(function(){
				scrollBar.removeClass('visible');
			},2000);

		}

		scrollBar.mouseover( function(){
			scrollBar.addClass('visible');
		});

		scrollBar.mouseleave( function(){
			if(self.isScrolling) return;
			scrollBar.removeClass('visible');
		});

		dropdown.on('mousewheel wheel', function(e) {
			showHandle();
			e.preventDefault();
			e.stopImmediatePropagation();

			var dy = e.originalEvent.wheelDeltaY || - e.originalEvent.deltaY,
				availableScrollY = ul.outerHeight() - dropdown.height(),
				n = Math.min(0, Math.max( -availableScrollY,  ul.position().top  + dy/10 ) );		

			self.setScrollValue( Math.abs(n)/availableScrollY );

		});			
		
		Hammer(scrollBar).on("touch", function(event) {
			$('body').css({
   				'-moz-user-select' : '-moz-none',
   				'-khtml-user-select' : 'none',
   				'-webkit-user-select' : 'none',
   				'user-select' : 'none'
			});
			scrollByEvent(event);
		});

		Hammer(scrollBar).on("drag tap", function(event) {
			self.isScrolling = true;
			var n = (event.gesture.center.pageY - scrollBar.offset().top + $(window).scrollTop() ) / scrollBar.height();
			self.setScrollValue( n );
			//scrollByEvent(event);
		});


		var startY = 0;
		Hammer(ul).on("dragstart", function(event) {

			startY = ul.position().top ;

		});


		Hammer(ul).on("drag", function(event) {
			event.gesture.preventDefault();
			self.isScrolling = true;
			
			/*$('body').on('touchmove', function(e){
			    e.preventDefault();
			});*/

			var dy = -startY - event.gesture.deltaY,
				availableScrollY = ul.outerHeight() - dropdown.height(),
				scrollVelocity = 0,
				n =  dy / availableScrollY;

			self.setScrollValue( n );
			//scrollByEvent(event, true);
		});


		/*
		Hammer(scrollBar).on("dragend", function(event) {
			isHandleVisible();
		});
		*/

		//add Event Listner to scrollbar and calculate handle position on mousedown and mousemove better use hammer.js
		/*
		$(this.view).on('keyup', function(){
			dropdownItems.each(....)
		})
		*/


		this._super();
		this.onResize();
	},
	toggleDropdown:function(){
	
		if(!this.isOpen)
			this.open();
		else
			this.close();
	},
	open: function(){
		if(this.isOpen) return;

		var self = this;
		this.isScrolling = false;
		this.isOpen = true;

		var scrollBar = $('.scrollbar', this.view), 
			dropdown = $( '.QaDropdown-inner',this.view );

		scrollBar.addClass('visible');
		dropdown.addClass('open');

		$('body').css({
			'-moz-user-select' : '-moz-none',
			'-khtml-user-select' : 'none',
			'-webkit-user-select' : 'none',
			'user-select' : 'none'
		});

		self.activate();

	},
	close: function(){

		if(!this.isOpen) return

		this.isScrolling = false;
		this.isOpen = false;
		var dropdown = $('.QaDropdown-inner',this.view);
		
		dropdown.removeClass('open');

		$('body').css({
			'-moz-user-select' : '-moz-element',
			'-khtml-user-select' : 'element',
			'-webkit-user-select' : 'element',
			'user-select' : 'element'
		});

		this.deactivate();

	},
	selectItemByIndex:function( i ){
		var self = this;
		var items = this.model.answers;
		i = Math.max(0,Math.min( items.length-1,i));
		this.index = i;
		var li = $('ul li', self.view );
		li.removeClass('selected');
		li.eq(i).addClass('selected');
		$('button',self.view).children( 'span' ).html( items[i] );
		this.setValue( [ i ] );
		this.scrollToIndex( i );

	},
	scrollToIndex: function(i){
		var scrollBar    = $('.scrollbar', this.view),
			handle = $('.handle',this.view),
			dropdown = $('.QaDropdown-inner',this.view),
			ul = $('ul',this.view);

			var pos = $('li',ul).eq( i ).position().top;

			this.setScrollValue( pos/ ( ul.outerHeight() - dropdown.height()) );

	},
	scrollByFilter:function( searchstring ){
		var self = this;
		var items = this.model.answers;
		var ul = $('ul', self.view );

		for( var i=0; i < items.length ; i++ ){
			
			if( items[i].toLowerCase().indexOf(searchstring) == 0){
				self.selectItemByIndex( i );
				return self.setScrollValue( $('li', ul).eq(i).position().top / ul.height() )
			} 

		}
		return 0
	},
	setScrollValue:function( n ){
		n = Math.max( 0, Math.min(1,n));
		this._srollValue = n;
		var scrollBar    = $('.scrollbar', this.view),
			handle = $('.handle',this.view),
			dropdown = $('.QaDropdown-inner',this.view),
			ul = $('ul',this.view);

			ul.css('top', -Math.round( n * (ul.outerHeight() - dropdown.height() ) ) );			
			handle.css('top', scrollBar.height() * n - handle.height() * n   );
		return 1
	},
	activate:function(){
		

		if(this._documentEvents) return;

		var self = this;

		self._documentEvents = { 
			click: function(){
	
				if(self.isScrolling){
					self.isScrolling = false;
					return;
				}
				self.close();
			},
			keypress:function(){
				var filter = '';
				var filterTimeout;
				
				//NAVIGATE BY TYPING COUNTRY NAME
				$(document).keypress(function(e) {			
					clearTimeout(filterTimeout);
					filter += String.fromCharCode(e.charCode);
					self.scrollByFilter( filter );
					filterTimeout = setTimeout(function() {
						filter = '';
					},500);
				});

			},
			keydown:function(){
				
				//NAVIGATE USING ARROW KEYS
				$(document).keydown(function(e) {


					switch(e.which) {

						case 38: // up
				
							self.selectItemByIndex( self.index-1 );
						break;

						case 40: // down
							self.selectItemByIndex( self.index+1 );
						break;

						case 13: // select
		
							self.close();
						break;

						default: return; // exit this handler for other keys
					}
					e.preventDefault(); // prevent the default action (scroll / move caret)
				});

			}
		};

		for(key in this._documentEvents){
			$(document).on( key, this._documentEvents[key] ); 
		}

		//$(button).on( 'click',function(){ self.close() }  );
		

	},
	deactivate: function(){
		if(!this._documentEvents) return

		for(key in this._documentEvents){

			$(document).off( key, this._documentEvents[key] ); 
		}

		delete this._documentEvents;

	},
	getScrollValue: function(){
		return this._scrollValue;
	},
	onResize:function(){
		//calculate handle size
		//reposition handle to 0 and scroll back to 0
	},


});

App.QaAccordionController = App.QaController.extend({
	init: function(){
		this.propertyMapping = {
			'name' : 'name',
			'children' : 'children',
			'value' : 'value'
		}
		this._super();
	},
	createChildren: function(){

		var self = this;
		var az = $('<div/>')
		az.addClass( 'accordion-az' );
		az.append( this.createIndicies( this.model.answers ) );
		az.on('click','a', function(e){
			e.preventDefault();
			var hash = $(this).attr('href').substr(1,2);
			
			var goTo = existing[ hash ] || null;
			if(goTo){
				var pos = $('.accordion > div',self.view ).eq( goTo ).position().top;
			}
		});

		this.view.append( az );

		var accordion = $('<div/>')
		accordion.addClass('accordion');
		accordion.append( this.createChildrenFor( this.model.answers ) )
		this.view.append( accordion );
	
		
		
	//ACCORDION 

	// for dev get there >> left: -13200px;

	/*
	
	$('.accordion div').on('click',function(){
		$(this).children().slideToggle();
		console.log("clicked"); 
	});
	
	$().on('click', function(){
	
	})

	$("#accordion > li > div").on('click', function(){
 
	    if(false == $(this).next().is(':visible')) {
	        $('#accordion ul').slideUp(300);
	    }
	    $(this).next().slideToggle(300);
	});
	 
	$('ul#accordion li ul li').on('click',function(){
		var city = $(this).text();
		$(this).parent().parent().find('span').text(" - " + city);
		$(this).parent().slideUp(300);
	});
	

	$('#accordion ul:eq(0)').show();

	}



	//SCROLL TO FUNCTION
	
	$('a[href*=#]:not([href=#])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});

	//
	*/

	},
	createIndicies:function( data ){
		var children = [];
		

		var letters = [ 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

		var sorted = data.sort( self.sort );

		self.existing = {};
		$.each( sorted, function(i){
			var letter = this.name.substr(0,1).toLowerCase();
			if( !(letter in existing) )
				existing[ letter ] = i;

		});

		$.each(letters, function( i  ){
			var a = $('<a href="#'+ this + '">' + this + '</a>');
			if( !(this in existing))
				a.addClass( 'disabled' );
			children.push( a );
		})
		
		return children;

	},
	createChildrenFor:function( data ){
		var items = [];

		var c, self=this;
		
		
		var sorted = data.sort(  self.sort ); 


		$.each( sorted, function( i ){

			c = $('<div/>');
			c.text( this[ self.propertyMapping['name'] ] );

			var children = this[ self.propertyMapping[ 'children' ] ] || [];

			if(  children && children.length > 0 ){

				c.append( self.createChildrenFor( children ) );
			}
			items.push( c );

		} )

		return items;

	},
	sort: function( a,b ){
		var aV = a['name'].toUpperCase(), bV = b['name'].toUpperCase();
		return aV > bV ? 1 : ( aV < bV ? -1 : 0 );
	}
});


App.QaCountryAccordionController = App.QaAccordionController.extend({
	init: function(){
		this._super();
		this.propertyMapping[ 'children' ] = 'answers';
	}
} )

