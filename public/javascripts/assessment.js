/* Polyfills */

if (typeof Object.create !== 'function') {
	Object.create = function(o, props) {
		function F() {}
		F.prototype = o;

		if (typeof(props) === "object") {
			for (prop in props) {
				if (props.hasOwnProperty((prop))) {
		 			F[prop] = props[prop];
				}
			}
		}

		return new F();
	};
}

/* Object Orientated Inheritance */

(function(){
	var OO = {};


	function extend( target, mixin){
		for(var k in mixin) {
			if (mixin.hasOwnProperty(k))
			  target[k] = mixin[k];
		}
	}

	function applyMixins( target, mixins ){
		for(var i = 0; i<mixins.length; i++){
			extend( target, mixins[i] );
		}
	}

	function functionWrap( f, superf  ){
		var N = function(){};

		var superWrap = function(){

			var ret, sup = this._super || N;

			this._super = superf || N; //set super class to superfunction or empty function

			ret = f.apply( this, arguments ); //execute child function

			this._super = sup; //set function to previous super function or empty function

			return ret; //return result
		}

		return superWrap;
	}


	var classMixin = (function(){
		function extend(){

			var Class = function(){},proto;
			proto = Object.create( this.prototype );
			applyMixins( proto, arguments );

			Class.prototype.constructor = Class;
			Class.__super__ = this.prototype;
			for (var k in proto) { 

				if(proto[k] instanceof Function){ 

					proto[k] = functionWrap( proto[k],this.prototype[k] );

				}
			}
			
			classMixin.call( Class );

			Class.prototype = proto;
			//console.log(this.prototype['init'] || null );
			return Class;
		};

		function create(){
			
			var inst = Object.create(this.prototype);
			applyMixins( inst, arguments );
			if('init' in inst && typeof inst.init == 'function' ){
				inst.init();
			}

			return inst 
		}
		return function(){
			this.extend = extend;
			this.create = create;
			return this;
		} 
	})();

	OO.Object = function(){};
	OO.Object.__super__ = Object.prototype;
	classMixin.call( OO.Object );
	extend( OO.Object.prototype,{ 
		init:function(){ return 1 }
	});
	window.OO = OO;
})();


/* Application, Session and Controller Base Class setup */

(function($){

	var App = OO.Object.create({
		init: function(){

			var self = this;
			
			this.view = $('.assessment');
			this.timeline = null;

			this.qaController = [];

			this.part = null;
			this.task = null;


			this.index = parseInt( this.getParams()['q'] || 0 ); //questions are simply handled by index
			
			
			$(document).ready( function(){ self.ready() });
			$(window).resize( function(){ self.onResize() });	
			
		},
		getParams:function(){
			var params = {};
			if (location.search) {
			    var parts = location.search.substring(1).split('&');

			    for (var i = 0; i < parts.length; i++) {
			        var nv = parts[i].split('=');
			        if (!nv[0]) continue;
			        params[nv[0]] = nv[1] || true;
			    }
			}
			return params
		},
		onResize: function(){
			var self = this;
			this.navigateTo( this.index,false );
			if(! $(self.view).hasClass('resize')){
				$(self.view).addClass( 'resize' );
				$('body').one('mouseover', function(){
					$(self.view).removeClass( 'resize' );
				});
			}
			//var slideWidth = $('.parts').width();
			//$('.questions').children().width( slideWidth );
		},
		navigateTo: function( i,animate){
			animate = animate == undefined ? true : animate;

			var questions = this.config.questions,
				cq = questions[i], //current question
				parts = $('.parts>*');

			$('.parts>*').each( function( pi ){ //pi == part index
				var p = $(this);

				if(pi == cq.part-1){//navigate part container to current part

					//slide to current part with or without animation
					if(animate){
						p.parent().velocity({'left' : -p.position().left +'px'}, { duration: 400, queue: false });
					}
					else{//animate
						p.parent().css({'left' : -p.position().left +'px'})
					}

				}

				//navigate tasks container in part to correct task 
				var t = pi == cq.part-1 ? $('.tasks>*',p).eq( cq.task-1 ) 
						: pi < cq.part-1 ? $('.tasks>*',p).last()
						: $('.tasks>*',p).first();

				if(pi == cq.part-1){
					var h = t.height() + t.parent().position().top ; //new height for main slider

						//resize sider main slider view
					if(animate){
							
							p.parent().velocity({'height': h + 'px'}, { duration: 300,queue: false });
							t.parent().velocity({'left' : -t.position().left +'px'}, { duration: 400, queue: false });
						
						
					}
					else{ 
						p.parent().css({ 'height': h + 'px'}); 
						t.parent().css( {'left' : -t.position().left +'px'} );
					}

				}
				else{
						t.parent().css( {'left' : -t.position().left +'px'} );
				}

				//animate to the current task
			

				$('.tasks>*',p).each( function(ti){
					var t = $( this );
					var flatTi = (pi+1) * 100 + (ti+1) * 10,
						qFlatTi = cq.part * 100 + cq.task * 10;

					q = flatTi == qFlatTi ? $('.questions>*').eq( i )
						: flatTi < qFlatTi ? $('.questions>*',t).last()
						: $('.questions>*',t).first();

					if(!animate){
						q.parent().css( {'left' : -q.position().left +'px'} );
					}
					else{
						q.parent().velocity({ 'left' : -q.position().left +'px'}, { duration: 400, queue: false });
					}
				
				});
			
			});
		},
		setConfig: function( conf ){
			this.config = conf;
			this.createChildren( this.view, this.config );
		},
		ready: function(){

				
		},
		sendAction: function(action, param ){
			var qc = this.currentQuestion();
			switch(action){
				case 'updateQuestion':
					if( qc.isValid()){
						$('.pager').addClass('next');
						App.session.updateQuestion( this.index, qc.getValue() );

					}
					else{
						$('.pager').removeClass('next');
					}
				break;
				case 'submitSession':

					App.session.save();
				break;
				case 'next':
					this.next();
				break;
				case 'previous':
					this.previous();
				break;
				case 'seek':
					this.seek( param );
			}
		},
		createChildren: function(view,config){

			var self = this;
			
			Hammer(this.view).on('swiperight', function(){ self.previous() } );
			Hammer($('.pager .previous')[0]).on('tap', function(){
				self.previous();
			});

			Hammer(this.view).on('swipeleft', function(){ self.next() } );
			Hammer($('.pager .next')[0]).on('tap', function(){
				self.next();
			});

			var timelineData = [];
			var prev;
			_.each( self.config.questions, function(question,i,questions){
				var path =  question.part + '/' + question.task;
				//console.log( path );
				//if(!prev || prev != path){ 
					timelineData.push(i);
					prev = path;
				//};
		
			});
			//var timelineData =  [0].concat( _.toArray( _.countBy( self.config.questions, function( q ){ return q.part + q.task } ) ) );
	
			this.timeline = App.TimelineController.create({
				view: $( '#timeline'),
				model: timelineData,
			});

			var questions = self.config.questions;//$('.questions',view).children();

			questions.forEach( function( q,i ){
		
				qac = self.createQuestionController( q );
				self.qaController.push(qac);
				self.session.updateQuestion(  i, qac.getValue() );
				var parent = $('<li/>');
				parent.append(qac.view );
				
				var questionsNode = $('.parts>*').eq( q.part - 1 ).find( '.tasks>*' ).eq( q.task - 1 ).children('.questions').first();
				questionsNode.append( parent );

			});

			$(this.view).velocity({'left': 0}, { duration: 1000, queue: false });
			self.onResize();
			
		},
		createQuestionController: function( q ){	
				var view;
				try{
					view = $( '#templates .' + q.type )[0].cloneNode(true);
				}
				catch(err){
					throw err;
				}
				
				if(!view)
					return;

				var className = ( q.type || '' ) + 'Controller';
				
				if( className in App){
					var CtrlClass = eval('App.'+className);
					var controller = CtrlClass.create({
						view: $(view),
						model:q,
					});

					controller.createChildren();
					controller.updateDisplay();
		
					return controller;
				}

				return null
			
		},
		getNodesForQuestion: function( i ){

			var q = this.config.questions[ i ],
				$p = $('.parts>*').eq( q.part-1 ),
				$t = $('.tasks>*',$p).eq( q.task-1 ),
				$q = $('.questions>*').eq( i );

			return { part: $p, task: $t, question: $q }
		},
		currentQuestion:function(){
			return this.qaController[ this.index ]
		},
		next:function(){
			var qac = this.currentQuestion();
			if(!qac.isValid())
				return;

			if( this.index == this.config.questions.length - 1){// all done
				this.session.saveAndForward();
			}
			this.seek( this.index + 1 );
			
		},
		previous:function(){
			this.seek( this.index - 1 );
		},
		seek:function( i ){

			var questions = App.config.questions;
			i = Math.max( Math.min( i, questions.length-1 ),0 );
			
			if(i == this.index) return;

			if( i > 0  )
				$('.pager').addClass('previous');
			else
				$('.pager').removeClass('previous');

			var qac = this.qaController[ i ];

			if(qac && qac.isValid() ){
				$('.pager').addClass('next');
			}else {
				$('.pager').removeClass('next');
			}
			
		//	$('body').velocity("scroll", { duration: 300, queue: false });
			this.navigateTo( i );
			this.timeline.seek( i );
			
			
			this.index = i;
			
		}
	});

	App.session = OO.Object.create( {
		init:function(){
			this.data = {
				assessment:[],
			};
		},
		getData: function(){
			return this.data;
		},
		saveAndForward:function(){



			$.ajax({
			    url: '/assessment', 
			    type: 'POST', 
			    contentType: 'application/json', 
			    data: JSON.stringify( this.data ) }
			).done(function(data){
				
				window.location = '/assessment/' + data.id;
			});

			//$.post('/assessment', JSON.stringify( this.data ) )	
			
		},
		updateQuestion: function(i,val){

			this.data.assessment[i] = val;
		
		}

	});



	window.App = App;

}(jQuery))